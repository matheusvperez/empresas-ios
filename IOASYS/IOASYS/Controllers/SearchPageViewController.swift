//
//  SearchPageViewController.swift
//  Empresas
//
//  Created by Matheus Perez on 10/09/20.
//  Copyright © 2020 Matheus Perez. All rights reserved.
//

import UIKit

class SearchPageViewController: UIViewController {

    @IBOutlet weak var searchField: UITextField!
    @IBOutlet weak var serachList: UITableView!
    @IBOutlet weak var searchCont: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()

        navigationBarSet()
    }
    
    let companies = ["x", "xy", "xyz"]
    
    func navigationBarSet() {
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.view.backgroundColor = .clear
        self.navigationItem.hidesBackButton = true
    }
    
}

extension SearchPageViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return companies.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let companie = companies[indexPath.row]
        
//        let cell = tableView.dequeueReusableCell(withIdentifier: ReusableCell, for: indexPath)as! DataCell
    }
    
    
}
