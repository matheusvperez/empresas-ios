//
//  ViewController.swift
//  IOASYS
//
//  Created by Matheus Perez on 09/09/20.
//  Copyright © 2020 Matheus Perez. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var fixImage: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        navigationBarSet()
        fixImageBorder()
        initializeHideKeyboard()
    }
    func fixImageBorder() {

        self.fixImage.layer.cornerRadius  = self.fixImage.frame.height/4
        self.fixImage.layer.masksToBounds = false
        self.fixImage.clipsToBounds = true
    }
    
    func navigationBarSet() {
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.view.backgroundColor = .clear
    }
    
    func initializeHideKeyboard(){
    let tap: UITapGestureRecognizer = UITapGestureRecognizer(
    target: self,
    action: #selector(dismissMyKeyboard))
    view.addGestureRecognizer(tap)
    }
    
    @objc func dismissMyKeyboard(){
    view.endEditing(true)
    }
}

